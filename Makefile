all: bin/servidor bin/cliente

bin/servidor: src/servidor.c
	gcc -Wall src/servidor.c -o $@

bin/cliente: src/cliente.c
	gcc -Wall src/cliente.c -o $@