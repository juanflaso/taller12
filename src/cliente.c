#include <sys/types.h>          /* some systems still require this */
#include <sys/stat.h>
#include <stdio.h>              /* for convenience */
#include <stdlib.h>             /* for convenience */
#include <stddef.h>             /* for offsetof */
#include <string.h>             /* for convenience */
#include <unistd.h>             /* for convenience */
#include <signal.h>             /* for SIG_ERR */ 
#include <netdb.h> 
#include <errno.h> 
#include <syslog.h> 
#include <sys/socket.h> 
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/resource.h>


#define TAMANOMAX 128 
#define MAXSLEEP 64

int connect_retry( int domain, int type, int protocol, 	const struct sockaddr *addr, socklen_t alen){
	
	int numsec, fd; 

	for (numsec = 1; numsec <= MAXSLEEP; numsec <<= 1) { 

		if (( fd = socket( domain, type, protocol)) < 0) 
			return(-1); 

		if (connect( fd, addr, alen) == 0) { 
			return(fd); 
		} 
		close(fd); 				
		if (numsec <= MAXSLEEP/2)

			sleep( numsec); 
	} 
	return(-1); 
}


int main( int argc, char *argv[]) { 

	int sockfd;

	if(argc == 1){
		printf("Uso: ./cliente <ip> <puerto> <string>\n");
		exit(-1);
	}

	if(argc != 4){
		printf( "por favor especificar un numero de puerto\n");
	}

	int puerto = atoi(argv[2]);
	char * stringAMandar = argv[3];


	
	struct sockaddr_in direccion_cliente;

	memset(&direccion_cliente, 0, sizeof(direccion_cliente));	
	direccion_cliente.sin_family = AF_INET;		
	direccion_cliente.sin_port = htons(puerto);		
	direccion_cliente.sin_addr.s_addr = inet_addr(argv[1]) ;

	if (( sockfd = connect_retry( direccion_cliente.sin_family, SOCK_STREAM, 0, (struct sockaddr *)&direccion_cliente, sizeof(direccion_cliente))) < 0) { 
		printf("falló conexión\n"); 
		exit(-1);
	} 

	int n = 0;
	char buf[TAMANOMAX];
	send( sockfd, stringAMandar, strlen(stringAMandar), 0); 
	while (( n = recv( sockfd, buf, TAMANOMAX, 0)) > 0) {
				
		write( STDOUT_FILENO, buf, n); 			
	}
	if (n < 0) 	
		printf(" recv error"); 


	return 0; 
}

