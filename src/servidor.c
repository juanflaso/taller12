#include <sys/types.h> 
#include <sys/stat.h>
#include <stdio.h>          
#include <stdlib.h>       
#include <stddef.h>           
#include <string.h>           
#include <unistd.h>            
#include <signal.h>          
#include <netdb.h> 
#include <errno.h> 
#include <syslog.h> 
#include <sys/socket.h> 
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/resource.h>
#include <ctype.h>

#define BUFLEN 128 
#define QLEN 10 

#ifndef HOST_NAME_MAX 
#define HOST_NAME_MAX 256 
#endif	


void toUpper(char* str) {
	for(int i =0; i<strlen(str); i++){
		str[i] = toupper((unsigned char) str[i]);
	}
}


int initserver(int type, const struct sockaddr *addr, socklen_t alen, int qlen){

	int fd;
	
	if((fd = socket(addr->sa_family, type, 0)) < 0)
		return -1;
		
	if(bind(fd, addr, alen) < 0) {
        close(fd);
        return -1;
    }

		
	if(type == SOCK_STREAM || type == SOCK_SEQPACKET){		
        if(listen(fd, qlen) < 0){
            close(fd);
            return -1;
        }
	}
	return fd;
}

int main( int argc, char *argv[]) { 
	int sockfd;

	if(argc != 3){
		printf("Uso: ./servidor <ip> <numero de puerto>\n");
		exit(-1);
	}

    char* direccionIP = argv[1];
	int puerto = atoi(argv[2]);
	
	struct sockaddr_in direccion_servidor;

	memset(&direccion_servidor, 0, sizeof(direccion_servidor));	

	direccion_servidor.sin_family = AF_INET;	
	direccion_servidor.sin_port = htons(puerto);		
	direccion_servidor.sin_addr.s_addr = inet_addr(direccionIP) ;

	
	if( (sockfd = initserver(SOCK_STREAM, (struct sockaddr *)&direccion_servidor, sizeof(direccion_servidor), 1024)) < 0){
		printf("Error al inicializar el servidor\n");	
	}

    char str[100];		

	while(1){
		int clfd = accept(sockfd, NULL, NULL);
        read(clfd,str,strlen(str));
        printf("%s", str);
		toUpper(str);
		send( clfd, str, strlen(str), 0);
		send( clfd, "\n", 1, 0);
		memset(str,0,strlen(str)); 
		close(clfd);
	}
	

	
	exit( 1); 
}
